
#include <nox/window/TrdRenderSdlWindowView.h>

#include <nox/app/IContext.h>
#include <nox/app/graphics/opengl/TrdOpenGlRenderer.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/DebugGeometryChange.h>

#include <assert.h>

namespace nox
{
namespace window
{

	TrdRenderSdlWindowView::TrdRenderSdlWindowView(app::IContext* applicationContext, const std::string& windowTitle):
	SdlWindowView(applicationContext, windowTitle, true),
	applicationContext(applicationContext),
	logicContext(nullptr),
	window(nullptr),
	listener("TrdRenderSdlWindowView")
{
	assert(applicationContext != nullptr);

	this->log = applicationContext->createLogger();
	this->log.setName("TrdRenderSdlWindowView");

	this->listener.addEventTypeToListenFor(logic::graphics::DebugGeometryChange::ID);
}

TrdRenderSdlWindowView::~TrdRenderSdlWindowView() = default;

bool TrdRenderSdlWindowView::initialize(logic::IContext* context)
{
	if (this->SdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), logic::event::ListenerManager::StartListening_t());

	this->logicContext = context;

	return true;
}

void TrdRenderSdlWindowView::render()
{
	assert(this->renderer != nullptr);

	this->renderer->onRender();
	SDL_GL_SwapWindow(this->window);
}

bool TrdRenderSdlWindowView::onWindowCreated(SDL_Window* window)
{
	this->window = window;

	this->renderer = std::unique_ptr<app::graphics::TrdOpenGlRenderer>(new app::graphics::TrdOpenGlRenderer());
	
	std::string shaderDirectory = "nox/shader/";

	if (this->renderer->init(this->applicationContext, shaderDirectory, this->getWindowSize()) == false)
	{
		this->log.fatal().raw("Failed to create OpenGL 3D renderer.");
		return false;
	}
	else
	{
		this->log.verbose().raw("Created OpenGL 3D renderer.");

		this->onRendererCreated(this->renderer.get());

		return true;
	}
}

void TrdRenderSdlWindowView::onSdlEvent(const SDL_Event& event)
{
	this->SdlWindowView::onSdlEvent(event);
}

void TrdRenderSdlWindowView::onDestroy()
{
	this->listener.stopListening();
}

void TrdRenderSdlWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	assert(this->renderer != nullptr);

	this->renderer->resizeWindow(size);
}

void TrdRenderSdlWindowView::onEvent(const std::shared_ptr<logic::event::Event>& event)
{

}

logic::IContext* TrdRenderSdlWindowView::getLogicContext()
{
	return this->logicContext;
}

app::IContext* TrdRenderSdlWindowView::getApplicationContext()
{
	return this->applicationContext;
}

}
}
