
#include <nox/logic/graphics/actor/TrdActorGraphics.h>

#include <nox/logic/graphics/event/TrdActorGraphicsCreated.h>

#include <nox/logic/IContext.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/app/graphics/TransformationNode.h>
#include <nox/util/json_utils.h>

#include <glm/gtc/matrix_transform.hpp>
#include <cassert>

namespace nox { namespace logic { namespace graphics
{

	const TrdActorGraphics::IdType TrdActorGraphics::NAME = "TrdGraphics";

	TrdActorGraphics::~TrdActorGraphics() = default;

const TrdActorGraphics::IdType& TrdActorGraphics::getName() const
{
	return NAME;
}

bool TrdActorGraphics::initialize(const Json::Value& componentJsonObject)
{
	Json::Value dataPathValue = componentJsonObject.get("dataPath", Json::nullValue);
	Json::Value nameValue = componentJsonObject.get("name", Json::nullValue);

	if (dataPathValue == Json::nullValue || nameValue == Json::nullValue)
	{
		return false;
	}

	this->dataPath = dataPathValue.asString();
	this->name = nameValue.asString();

	this->broadcastAiSceneCreation();

	return true;
}

void TrdActorGraphics::onCreate()
{
}

void TrdActorGraphics::onDestroy()
{
}

void TrdActorGraphics::serialize(Json::Value &componentObject)
{
}


void TrdActorGraphics::broadcastAiSceneCreation()
{
	const auto actorGraphicsCreatedEvent = std::make_shared<nox::logic::actor::TrdActorGraphicsCreated>(this->getOwner(), this->dataPath, this->name);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(actorGraphicsCreatedEvent);
}

std::string TrdActorGraphics::getModelName()
{
	return this->name;
}

} } }
