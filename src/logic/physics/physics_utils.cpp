/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/physics_utils.h>
#include <nox/util/json_utils.h>
#include <nox/util/boost_utils.h>
#include <boost/geometry/strategies/strategies.hpp>
#include <boost/geometry/algorithms/correct.hpp>

namespace nox { namespace logic { namespace physics
{

bool parseBodyShapeJson(const Json::Value& shapeJson, BodyShape& shape)
{
	const Json::Value& shapeTypeValue = shapeJson["type"];

	if (shapeTypeValue.isNull())
	{
		return false;
	}

	const std::string& shapeType = shapeTypeValue.asString();

	if (shapeType == "circle")
	{
		shape.type = ShapeType::CIRCLE;
		shape.circleRadius = shapeJson.get("radius", 1.0f).asFloat();
	}
	else if (shapeType == "polygon")
	{
		shape.type = ShapeType::POLYGON;

		const Json::Value& vertexList = shapeJson["vertexList"];

		if (vertexList.isNull())
		{
			return false;
		}

		if (vertexList.size() < 3)
		{
			return false;
		}

		shape.polygons.push_back(util::BoostPolygon());
		util::BoostPolygon& polygon = shape.polygons.front();

		for (const Json::Value& jsonVertex : vertexList)
		{
			glm::vec2 vertex;

			vertex.x = jsonVertex.get("x", 0.0f).asFloat();
			vertex.y = jsonVertex.get("y", 0.0f).asFloat();

			boost::geometry::append(polygon, vertex);
		}

		boost::geometry::correct(polygon);
	}
	else if (shapeType == "box")
	{
		shape.type = ShapeType::RECTANGLE;
		shape.box.setLowerBound(util::parseJsonVec(shapeJson["lowerBound"], glm::vec2(0.0f, 0.0f)));
		shape.box.setUpperBound(util::parseJsonVec(shapeJson["upperBound"], glm::vec2(1.0f, 1.0f)));
	}
	else if (shapeType == "none")
	{
		shape.type = ShapeType::NONE;
	}
	else
	{
		return false;
	}

	return true;
}

BodyShape transformShape(const BodyShape& shape, const glm::vec2& position, const glm::vec2& scale, float rotation)
{
	BodyShape transformedShape;
	transformedShape.circleRadius = shape.circleRadius;
	transformedShape.type = shape.type;

	if (shape.type == ShapeType::POLYGON)
	{
		for (const util::BoostPolygon& polygon : shape.polygons)
		{
			transformedShape.polygons.push_back(util::transformPolygon(polygon, position, rotation, scale));
		}
	}

	return transformedShape;
}

BodyShape makeBoxShape(const glm::vec2& size)
{
	BodyShape boxShape;
	boxShape.type = ShapeType::POLYGON;

	util::BoostPolygon boxPolygon;

	boxPolygon.outer().push_back(glm::vec2(-size.x / 2.0f, -size.y / 2.0f));
	boxPolygon.outer().push_back(glm::vec2(size.x / 2.0f, -size.y / 2.0f));
	boxPolygon.outer().push_back(glm::vec2(size.x / 2.0f, size.y / 2.0f));
	boxPolygon.outer().push_back(glm::vec2(-size.x / 2.0f, size.y / 2.0f));
	boxPolygon.outer().push_back(glm::vec2(-size.x / 2.0f, -size.y / 2.0f));

	boxShape.polygons.push_back(boxPolygon);

	return boxShape;
}

util::Mask<PhysicalBodyType> staticAndKinematicBodyTypes()
{
	return util::Mask<PhysicalBodyType>{PhysicalBodyType::STATIC, PhysicalBodyType::KINEMATIC};
}

util::Mask<PhysicalBodyType> staticAndDynamicBodyTypes()
{
	return util::Mask<PhysicalBodyType>{PhysicalBodyType::STATIC, PhysicalBodyType::DYNAMIC};
}

util::Mask<PhysicalBodyType> kinematicAndDynamicBodyTypes()
{
	return util::Mask<PhysicalBodyType>{PhysicalBodyType::KINEMATIC, PhysicalBodyType::DYNAMIC};
}

util::Mask<PhysicalBodyType> allBodyTypes()
{
	return util::Mask<PhysicalBodyType>{PhysicalBodyType::STATIC, PhysicalBodyType::KINEMATIC, PhysicalBodyType::DYNAMIC};
}

} } }
