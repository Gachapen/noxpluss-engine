/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/actor/TrdActorPhysics.h>

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/util/json_utils.h>
#include <nox/util/math/angle.h>
#include <boost/geometry/strategies/strategies.hpp>
#include <boost/geometry/algorithms/area.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <nox/logic/physics/Simulation.h>

namespace nox { namespace logic { namespace physics
{

const TrdActorPhysics::IdType TrdActorPhysics::NAME = "TrdPhysics";

const TrdActorPhysics::IdType& TrdActorPhysics::getName() const
{
	return TrdActorPhysics::NAME;
}




bool TrdActorPhysics::initialize(const Json::Value& componentJsonObject)
{
	//Todo: create a data structure for storing physics data

	float density = componentJsonObject.get("density", 0.0f).asFloat();

	return true;
}
	 
void TrdActorPhysics::onCreate()
{

}
	 
	 
void TrdActorPhysics::onDestroy()
{

}
	 
	 
void TrdActorPhysics::onComponentEvent(const std::shared_ptr<event::Event>& event)
{

}
	 
	 
void TrdActorPhysics::onUpdate(const Duration& deltaTime)
{

}
	 
void TrdActorPhysics::onActivate()
{

}
	 
void TrdActorPhysics::onDeactivate()
{

}


void TrdActorPhysics::serialize(Json::Value& componentObject)
{
}


} } }
