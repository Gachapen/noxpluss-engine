/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/resource/loader/JsonLoader.h>

namespace nox { namespace app
{
namespace resource
{

JsonLoader::JsonLoader() = default;

JsonLoader::JsonLoader(log::Logger logger):
	log(std::move(logger))
{
	this->log.setName("JsonLoader");
}

void JsonLoader::setLogger(log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("JsonLoader");
}

const char* JsonLoader::getPattern() const
{
	return "*.json";
}

bool JsonLoader::useRawFile() const
{
	return false;
}

unsigned int JsonLoader::getLoadedResourceSize(const char* /*rawBuffer*/, const unsigned int rawSize) const
{
	// FIXME: This is probably incorrect.
	return rawSize;
}

bool JsonLoader::loadResource(const util::Buffer<char> inputBuffer, util::Buffer<char>& /*outputBuffer*/, std::unique_ptr<IExtraData>& extraData) const
{
	auto jsonExtraData = std::unique_ptr<JsonExtraData>(new JsonExtraData());

	if (jsonExtraData->parseJson(inputBuffer.data, inputBuffer.size, this->log) == false)
	{
		return false;
	}

	extraData = std::move(jsonExtraData);

	return true;
}

}
} }
