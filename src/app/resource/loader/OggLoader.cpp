/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/resource/loader/OggLoader.h>

#include <cstring>
#include <stdlib.h>
#include <cassert>
#include <ogg/ogg.h>
#include <vorbis/vorbisfile.h>
#include <vorbis/vorbisenc.h>
#include <vorbis/codec.h>

namespace nox { namespace app
{
namespace resource
{

struct OggMemoryFile
{
	const char* dataPtr;
	std::size_t dataSize;
	std::size_t dataRead;

	OggMemoryFile(void)
	{
		dataPtr = nullptr;
		dataSize = 0;
		dataRead = 0;
	}
};

static size_t vorbisRead(void* data_ptr, size_t byteSize, size_t sizeToRead, void* data_src);
static int vorbisSeek(void* data_src , ogg_int64_t offset, int origin);
static int vorbisClose(void* src);
static long vorbisTell(void* data_src);

bool OggLoader::useRawFile() const
{
	return false;
}

unsigned int OggLoader::getLoadedResourceSize(const char* rawBuffer, const unsigned int rawSize) const
{
	OggVorbis_File vf;
	ov_callbacks oggCallbacks;

	OggMemoryFile* vorbisMemoryFile = new OggMemoryFile;
	vorbisMemoryFile->dataRead = 0;
	vorbisMemoryFile->dataSize = rawSize;
	vorbisMemoryFile->dataPtr = rawBuffer;

	oggCallbacks.read_func = &vorbisRead;
	oggCallbacks.close_func = &vorbisClose;
	oggCallbacks.seek_func = &vorbisSeek;
	oggCallbacks.tell_func = &vorbisTell;

	int ov_ret = ov_open_callbacks(vorbisMemoryFile, &vf, nullptr, 0, oggCallbacks);

	if (ov_ret < 0)
	{
		return 0;
	}
	else
	{
		vorbis_info* vi = ov_info(&vf, -1);

		ogg_int64_t bytes = ov_pcm_total(&vf, -1);
		bytes *= 2 * vi->channels;

		ov_clear(&vf);
		if (vorbisMemoryFile)
		{
			delete vorbisMemoryFile;
			vorbisMemoryFile = nullptr;
		}

		return static_cast<unsigned int>(bytes);
	}
}

bool OggLoader::loadResource(const util::Buffer<char> inputBuffer, util::Buffer<char>& outputBuffer, std::unique_ptr<IExtraData>& extraData) const
{
	extraData = this->parseOgg(inputBuffer, outputBuffer);

	if (extraData == nullptr)
	{
		return false;
	}

	return true;
}

const char* OggLoader::getPattern() const
{
	return "*.ogg";
}

std::unique_ptr<SoundExtraData> OggLoader::parseOgg(const util::Buffer<char> inputBuffer, util::Buffer<char>& outputBuffer) const
{
	std::unique_ptr<SoundExtraData> oggExtraData;;

	OggVorbis_File vf;
	ov_callbacks oggCallbacks;

	std::unique_ptr<OggMemoryFile> vorbisMemoryFile(new OggMemoryFile);
	vorbisMemoryFile->dataRead = 0;
	vorbisMemoryFile->dataSize = inputBuffer.size;
	vorbisMemoryFile->dataPtr = inputBuffer.data;

	oggCallbacks.read_func = &vorbisRead;
	oggCallbacks.close_func = &vorbisClose;
	oggCallbacks.seek_func = &vorbisSeek;
	oggCallbacks.tell_func = &vorbisTell;

	int ov_ret = ov_open_callbacks(vorbisMemoryFile.get(), &vf, NULL, 0, oggCallbacks);

	if (ov_ret >= 0)
	{
		vorbis_info* vi = ov_info(&vf, -1);

		long size = 4096 * 16;
		long pos = 0;
		int sec = 0;
		long ret = 1;

		ogg_int64_t bytes = ov_pcm_total(&vf, -1);
		bytes *= 2 * vi->channels;

		if (bytes == outputBuffer.size && vi->channels > 0 && vi->rate > 0)
		{
			while (ret && pos < bytes)
			{
				ret = ov_read(&vf, outputBuffer.data + pos, static_cast<int>(size), 0, 2, 1, &sec);
				pos += ret;
				if ((static_cast<long>(bytes) - pos) < size)
				{
					size = static_cast<long>(bytes) - pos;
				}
			}

			int lengthInMilli = static_cast<int>(1000.f * static_cast<float>(ov_time_total(&vf, -1)));

			oggExtraData = std::unique_ptr<SoundExtraData>(new SoundExtraData());
			oggExtraData->setLengthInMilli(lengthInMilli);

			AudioData ogg;
			ogg.size = sizeof(ogg);
			ogg.nChannels = static_cast<unsigned int>(vi->channels);
			ogg.bitsPerSample = 16;
			ogg.samplesPerSec = static_cast<unsigned long int>(vi->rate);
			ogg.avgBytesPerSec = ogg.samplesPerSec * static_cast<unsigned long int>(ogg.nChannels) * 2;
			ogg.blockAlign = 2 * ogg.nChannels;
			oggExtraData->setFormat(ogg);
		}

		ov_clear(&vf);
	}

	return oggExtraData;
}

size_t vorbisRead(void* data_ptr, size_t byteSize, size_t sizeToRead, void* data_src)
{
	OggMemoryFile* pVorbisData = static_cast<OggMemoryFile*>(data_src);
	if (pVorbisData == nullptr)
	{
		return 0;
	}

	size_t actualSizeToRead;
	size_t spaceToEOF = pVorbisData->dataSize - pVorbisData->dataRead;

	if ((sizeToRead * byteSize) < spaceToEOF)
	{
		actualSizeToRead = (sizeToRead * byteSize);
	}
	else
	{
		actualSizeToRead = spaceToEOF;
	}

	if (actualSizeToRead)
	{
		memcpy(data_ptr, pVorbisData->dataPtr + pVorbisData->dataRead, actualSizeToRead);
		pVorbisData->dataRead += actualSizeToRead;
	}
	return actualSizeToRead;
}

int vorbisSeek(void* data_src , ogg_int64_t offset, int origin)
{
	OggMemoryFile* pVorbisData = static_cast<OggMemoryFile*>(data_src);
	if (pVorbisData == nullptr)
	{
		return -1;
	}

	switch (origin)
	{
		case SEEK_SET:
		{
			size_t actualOffset;
			actualOffset = (pVorbisData->dataSize >= static_cast<size_t>(offset)) ? static_cast<size_t>(offset) : pVorbisData->dataSize;
			pVorbisData->dataRead = actualOffset;
			break;
		}
		case SEEK_CUR:
		{
			size_t spaceToEOF = pVorbisData->dataSize - pVorbisData->dataRead;

			size_t actualOffset;
			actualOffset = (static_cast<size_t>(offset) < spaceToEOF) ? static_cast<size_t>(offset) : spaceToEOF;

			pVorbisData->dataRead += actualOffset;
			break;
		}
		case SEEK_END:
		{
			pVorbisData->dataRead = pVorbisData->dataSize + 1;
			break;
		}

		default:
		{
			assert(false && "Bad parameter for 'origin', requires same as fseek,");
			break;
		}
	};
	return 0;
}

int vorbisClose(void* /*src*/)
{
	return 0;
}

long vorbisTell(void* data_src)
{
	OggMemoryFile* pVorbisData = static_cast<OggMemoryFile*>(data_src);
	if (pVorbisData == nullptr)
	{
		return -1L;
	}
	return static_cast<long>(pVorbisData->dataRead);
}

}
} }
