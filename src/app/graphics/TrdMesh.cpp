
//#include <GL/glew.h>

#include <nox\app\graphics\TrdMesh.h>

namespace nox 
{
	namespace app
	{
		namespace graphics
		{
			TrdMesh::TrdMesh(std::vector<vertexData>* vd, std::vector<unsigned int>* id, std::vector<textureData> * td)
			{

				this->data = *vd;
				this->indices = *id;

				if (td)
				{
					this->textures = *td;
				}

				glGenBuffers(1, &VBO);
				glBindBuffer(GL_ARRAY_BUFFER, VBO);
				glBufferData(GL_ARRAY_BUFFER, this->data.size() * sizeof(vertexData), &this->data[0], GL_STATIC_DRAW);

				glGenBuffers(1, &IBO);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(unsigned int), &this->indices[0], GL_STATIC_DRAW);


				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}

			TrdMesh::~TrdMesh()
			{
				glDeleteBuffers(1, &VBO);
				glDeleteBuffers(1, &IBO);
			}

			void TrdMesh::draw(unsigned int programId)
			{
				// https://www.youtube.com/watch?v=ClqnhYAYtcY at 34:40

				

				std::string str = "texture";
				// Bind textures
				for (int i = 0; i < textures.size(); i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					glBindTexture(GL_TEXTURE_2D, textures[i].id);
					glUniform1i(glGetUniformLocation(programId, (str + (char)(i + '0')).c_str()), i);
				}

				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
				glEnableVertexAttribArray(2);
				glEnableVertexAttribArray(3);
				glEnableVertexAttribArray(4);

				glBindBuffer(GL_ARRAY_BUFFER, VBO);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData), 0);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData), (void*)(3 * sizeof(float)));
				glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData), (void*)(6 * sizeof(float)));
				glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData), (void*)(9 * sizeof(float)));
				glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(vertexData), (void*)(12 * sizeof(float)));

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
				glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);

				glDisableVertexAttribArray(0);
				glDisableVertexAttribArray(1);
				glDisableVertexAttribArray(2);
				glDisableVertexAttribArray(3);
				glDisableVertexAttribArray(4);

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

			}
		}
	}
}