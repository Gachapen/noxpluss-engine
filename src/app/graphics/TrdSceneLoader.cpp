
//#include <SDL2\SDL_image.h>		// To load textures (TODO)
#include <GL\glew.h>
#include "nox\app\graphics\TrdSceneLoader.h"

namespace nox {
	namespace app
	{
		namespace graphics
		{

			TrdSceneLoader::TrdSceneLoader(std::string fileName)
			{
				Assimp::Importer importer;
				const aiScene* scene = importer.ReadFile(fileName, aiProcess_GenSmoothNormals | aiProcess_Triangulate | aiProcess_CalcTangentSpace | aiProcess_FlipUVs);


				if (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
				{
					std::cout << "TODO: nox logging here. Error: The file was not opened: " << fileName << std::endl;
					return;
				}

				// Load all the meshed in scene
				recursiveProcess(scene->mRootNode, scene);
			}


			TrdSceneLoader::~TrdSceneLoader()
			{
				for (int i = 0; i < meshes.size(); i++)
				{
					delete meshes[i];
				}
			}


			void TrdSceneLoader::draw(unsigned int programId)
			{
				for (int i = 0; i < meshes.size(); i++)
				{
					meshes[i]->draw(programId);
				}
			}

			void TrdSceneLoader::recursiveProcess(aiNode* node, const aiScene* scene)
			{
				// https://www.youtube.com/watch?v=ClqnhYAYtcY at 53:40

				// Process
				for (int i = 0; i < node->mNumMeshes; i++)
				{
					aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
					processMesh(mesh, scene);
				}


				// Recursion
				for (int i = 0; i < node->mNumChildren; i++)
				{
					recursiveProcess(node->mChildren[i], scene);
				}


			}

			void TrdSceneLoader::processMesh(aiMesh* mesh, const aiScene* scene)
			{
				std::vector<vertexData> data;
				std::vector<unsigned int> indices;
				std::vector<textureData> textures;

				aiColor4D col;
				aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
				aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &col);
				glm::vec3 defaultColor(col.r, col.g, col.b);

				// Get vertex data

				for (int i = 0; i < mesh->mNumVertices; i++)
				{
					vertexData tmp;
					glm::vec3 tmpVec;



					// Position
					tmpVec = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
					tmp.position = tmpVec;



					// normals
					tmpVec = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };
					tmp.normal = tmpVec;



					// tangents
					if (mesh->mTangents)
					{
						tmpVec = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
					}
					else
					{
						tmpVec = { 1, 0, 0 };
					}
					tmp.tangent = tmpVec;



					// colors
					if (mesh->mColors[0])
					{
						tmpVec = { mesh->mColors[0][i].r, mesh->mColors[0][i].g, mesh->mColors[0][i].b };
					}
					else
					{
						tmpVec = defaultColor;
					}
					tmp.color = tmpVec;


					// UV coordinates
					if (mesh->mTextureCoords[0])
					{
						tmpVec.x = mesh->mTextureCoords[0][i].x;
						tmpVec.y = mesh->mTextureCoords[0][i].y;
					}
					else
					{
						tmpVec.x = 0.0;
						tmpVec.y = 0.0;
					}
					tmp.uvPosition.x = tmpVec.x;
					tmp.uvPosition.y = tmpVec.y;

					data.push_back(tmp);
				}


				// Get indecies

				for (int i = 0; i < mesh->mNumFaces; i++)
				{
					aiFace face = mesh->mFaces[i];

					for (int j = 0; j < face.mNumIndices; j++)		// 0 - 2
					{
						indices.push_back(face.mIndices[j]);
					}
				}


				// Get textures

				for (int i = 0; i < mat->GetTextureCount(aiTextureType_DIFFUSE); i++)		// normally between 0 and 5
				{
					aiString str;
					mat->GetTexture(aiTextureType_DIFFUSE, i, &str);
					textureData tmp;
					tmp.id = loadTexture(str.C_Str());
					tmp.type = 0;
					textures.push_back(tmp);
				}

				meshes.push_back(new TrdMesh(&data, &indices, &textures));
			}

			unsigned int TrdSceneLoader::loadTexture(const char* fileName)
			{
				// TODO
				return 0;
			}

			std::vector<TrdMesh*>& TrdSceneLoader::getMeshes()
			{
				return meshes;
			}
		}
	}
}