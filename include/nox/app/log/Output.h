/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_LOG_OUTPUT_H_
#define NOX_APP_LOG_OUTPUT_H_

#include <nox/common/api.h>

#include <string>

namespace nox { namespace app
{
namespace log
{

class Message;


class NOX_API Output
{
public:
	Output();
	virtual ~Output();

	/**
	 * Set the format for the log output.
	 *
	 * The format is a plain string with variables defined by "${}".
	 *
	 * ### Available variables ###
	 * - message: The message string.
	 * - loglevel: The level at which the message was logged.
	 * - loggername: The name of the logger that logged the message.
	 *
	 * ### Examples ###
	 * - "[${loglevel}][${loggername}] ${message}": "[info][SomeLogger] This is some logged message."
	 *
	 * @param format The format string for the output.
	 */
	void setOutputFormat(const std::string& format);

	virtual void log(const Message& message) = 0;

protected:
	std::string getFormattedMessage(const Message& message);

private:
	std::string getFormattedString(const Message& message, const std::string& format) const;

	std::string outputFormat;
};

}
} }

#endif
