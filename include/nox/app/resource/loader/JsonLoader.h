/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_JSONLOADER_H_
#define NOX_APP_RESOURCE_JSONLOADER_H_

#include <nox/app/resource/loader/ILoader.h>
#include <nox/common/api.h>

namespace nox { namespace app
{
namespace resource
{

/**
 * Loads json resources.
 * The json data is stored in JsonResourceExtraData provided by ResourceHandle::getExtra.
 */
class NOX_API JsonLoader: public ILoader
{
public:
	JsonLoader();
	JsonLoader(log::Logger logger);

	void setLogger(log::Logger logger);

	const char* getPattern() const override;
	bool useRawFile() const override;
	unsigned int getLoadedResourceSize(const char *rawBuffer, const unsigned int rawSize) const override;
	bool loadResource(const util::Buffer<char> inputBuffer, util::Buffer<char>& outputBuffer, std::unique_ptr<IExtraData>& extraData) const override;

private:
	mutable log::Logger log;
};

}
} }

#endif
