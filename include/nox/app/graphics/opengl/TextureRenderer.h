/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TEXTURERENDERER_H_
#define NOX_APP_GRAPHICS_TEXTURERENDERER_H_

#include "TextureQuad.h"
#include <GL/glew.h>
#include <nox/util/math/Box.h>
#include <map>
#include <unordered_map>
#include <list>
#include <vector>

namespace nox { namespace app
{
namespace graphics
{

class RenderData;

/**
 * Used for rendering textures fast.
 * This renderer has several render levels which will be rendererd
 * in order so that higher levels will appear over lower levels.
 * A specified number of lower levels will be static and never change
 * their data. The other levels will be streamed and changing the data
 * a lot is OK.
 */
class TextureRenderer
{
public:
	/**
	 * Handle to data reserved from the TextureRenderer.
	 * Used this to modify or remove data.
	 */
	class DataHandle
	{
	public:
		DataHandle();
		DataHandle(unsigned int id);
		DataHandle(const DataHandle& other);
		DataHandle& operator = (const DataHandle& other);

		bool operator == (const DataHandle& other) const;
		bool operator < (const DataHandle& other) const;

		/**
		 * Check if the handle refers to valid data.
		 * @return true if valid, otherwise false.
		 */
		bool isValid() const;

	private:
		int id;
	};

	/**
	 * Create the renderer.
	 * @param numRenderLevels Total number of render levels.
	 */
	TextureRenderer(unsigned int numRenderLevels);

	~TextureRenderer();

	/**
	 * Initialize the renderer.
	 * @param vertexAttribLocation Location of the shader vertex coordinate attribute.
	 * @param textureAttribLocation Location of the shader texture coordinate attribute.
	 */
	void init(RenderData& renderData, GLuint vertexAttribLocation, GLuint textureAttribLocation, GLuint colorLocation, GLuint lightLuminanceLocation);

	/**
	 * Request space for texture data.
	 * The space will be allocated and data can then be set with drawData.
	 * @param numQuads The number of quads to request.
	 * @param renderLevel Level to render the data at.
	 * @return Handle to the space allocated.
	 */
	DataHandle requestDataSpace(unsigned int numQuads, unsigned int renderLevel);

	/**
	 * Set the data to be rendererd for a handler.
	 * The handle mus be a valid handle returned by requestDataSpace.
	 * @param handle Handle to the data space.
	 * @param data Data to set.
	 * @return true if valid handle and data set, otherwise false.
	 */
	bool setData(const DataHandle& handle, const std::vector<TextureQuad::RenderQuad>& data);

	/**
	 * Delete data referred by handle.
	 * Handle will be invalidated after deletion.
	 * @param handle Handle to delete data from.
	 * @return true if handle is valid and data deleted, otherwise false.
	 */
	bool deleteData(DataHandle& handle);

	/**
	 * Draw the data.
	 */
	void drawData(RenderData& renderData, unsigned int startRenderLevel, unsigned int endRenderLevel);

	void drawData(RenderData& renderData);

	void handleIo(RenderData& renderData);

	void enableCull(bool enable);
	void setCullArea(const math::Box<glm::vec2>& area, const float rotation);

private:
	using TexturesIndex = std::vector<TextureQuad::RenderQuad>::size_type;
	using RenderIndicesIndex = std::vector<GLuint>::size_type;

	struct RenderLevelData
	{
		RenderLevelData():
			dataSize(0)
		{}

		TexturesIndex dataSize;
	};

	struct TextureBlock
	{
		TexturesIndex start;
		TexturesIndex length;
	};

	/**
	 * Data about the space allocated for handle.
	 */
	struct TextureRenderData
	{
		TexturesIndex startIndex;			//!< Offset from the level to the data space.
		TexturesIndex numQuads;		//!< Length of the data space (num quads).
		unsigned int renderLevel;	//!< Level to render data space at.

		bool operator<(const TextureRenderData& other) const;
		bool operator==(const TextureRenderData& other) const;
	};

	using FreeList = std::vector<TextureBlock>;
	using FreeListIterator = FreeList::iterator;

	void reserveSpace(TextureRenderData& handleData);
	void fillRenderData(const std::vector<TextureQuad::RenderQuad>& fillFrom, std::vector<TextureQuad::RenderQuad>& fillTo, const TextureRenderData& handleData);
	void freeSpace(const TextureRenderData& dataHandle);
	FreeListIterator findFreeBlock(const TextureRenderData& data);

	RenderIndicesIndex fillQuadRenderIndices(const TexturesIndex quadIndex, const RenderIndicesIndex startIndex);

	/**
	 * Fill render indices from startIndex, based on data.
	 * @param data Data for quads to fill indices from.
	 * @param startIndex Index to start filling indices.
	 * @return The next index after the filled indices.
	 */
	RenderIndicesIndex fillDataRenderIndices(const TextureRenderData& data, const RenderIndicesIndex startIndex);

	RenderIndicesIndex fillIndices();
	RenderIndicesIndex fillIndicesCulled();
	std::pair<TexturesIndex, TexturesIndex> getDataRange(const unsigned int startLevel, const unsigned int endLevel) const;

	const GLuint INDICES_PER_QUAD = 6;

	const unsigned int NUM_RENDER_LEVELS;

	unsigned int handleIdIncrementor;
	std::map<DataHandle, TextureRenderData> handleData;

	std::map<unsigned int, RenderLevelData> originalRenderLevels;
	std::map<unsigned int, RenderLevelData> renderLevels;
	std::list<TextureRenderData> sortedRenderData;
	std::vector<TextureBlock> freeList;

	std::vector<TextureQuad::RenderQuad> textureQuads;
	TexturesIndex numberOfRenderQuads;

	std::vector<GLuint> renderIndices;

	GLuint textureRenderVao;
	GLuint indexBuffer;
	GLuint vertexBuffer;

	bool vertexDataChanged;
	bool dataSizeChanged;

	GLsizeiptr indexBufferDataSize;
	GLsizeiptr vertexBufferDataSize;

	bool cullEnabled;
	math::Box<glm::vec2> cullArea;
	float cullRotation;
};

}
} }

#endif
