/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_LIGHT_H_
#define NOX_APP_GRAPHICS_LIGHT_H_

#include <string>
#include <glm/glm.hpp>
#include "GeometrySet.h"

namespace nox { namespace app
{
namespace graphics
{

class Triangle;

struct LightRenderTriangle
{
	glm::vec2 lowerPoint;
	glm::vec2 upperPoint;
	
	float lowerAngle;
	float upperAngle;
	
	Triangle* geoemtry;
};

/**
 * Representation of a light.
 */
class Light
{
public:
	enum class RenderHint
	{
		DYNAMIC,
		STATIC
	};

	Light();
	
	void setRange(float range);
	void setPosition(const glm::vec2& position);
	void setPositionOffset(const glm::vec2& offset);
	void setColor(const glm::vec4& color);
	void setEnabled(bool enabled);
	void setCastDirection(float directionAngle);
	
	const std::string& getName() const;
	void setName(const std::string& name);
	const glm::vec4& getColor() const;
	const glm::vec2& getPosition() const;
	const glm::vec2& getPositionOffset() const;
	float getRange() const;
	bool isEnabled() const;
	float getCastDirection() const;

	void setGeometryUpdated();
	void setGeometryOutdated();
	bool needsGeometryUpdate() const;

	bool needsRenderUpdate() const;
	void setRenderingUpdated();

	void setEffect(const bool state);
	bool isEffect() const;
	
	GeometrySet lightArea;
	std::vector<LightRenderTriangle> lightAreaTriangles;
	
	float alphaFallOff;
	float coneAngleRadian;
	
	unsigned int numRays;
	
	bool isRadialLight;
	bool castShadows;
	bool shouldRotateWithPlayer;

	RenderHint renderHint;
	
private:
	std::string name;
	
	glm::vec4 color;
	glm::vec2 position;
	glm::vec2 positionOffset;
	
	float range;
	float castDirectionRadian;
	
	bool enabled;
	bool geometryChanged;
	bool geometryOutdated;

	bool effect;
};

inline void Light::setName(const std::string &name)
{
	this->name = name;
}

inline const std::string& Light::getName() const
{
	return this->name;
}

inline void Light::setRange(float range)
{
	this->range = range;
	this->geometryOutdated = true;
}

inline void Light::setPosition(const glm::vec2& position)
{
	this->position = position;
	this->geometryOutdated = true;
}

inline void Light::setPositionOffset(const glm::vec2& offset)
{
	this->positionOffset = offset;
	this->geometryOutdated = true;
}

inline void Light::setColor(const glm::vec4& color)
{
	this->color = color;
	this->geometryOutdated = true;
}

inline void Light::setEnabled(bool enabled)
{
	if (this->enabled != enabled)
	{
		this->geometryOutdated = true;
	}

	this->enabled = enabled;
}

inline void Light::setCastDirection(float directionAngle)
{
	this->castDirectionRadian = directionAngle;
	this->geometryOutdated = true;
}

inline void Light::setGeometryUpdated()
{
	this->geometryOutdated = false;
	this->geometryChanged = true;
}

inline void Light::setGeometryOutdated()
{
	this->geometryOutdated = true;
}

inline bool Light::needsGeometryUpdate() const
{
	return this->geometryOutdated;
}

inline bool Light::needsRenderUpdate() const
{
	return this->geometryChanged;
}

inline void Light::setRenderingUpdated()
{
	this->geometryChanged = false;
}

inline const glm::vec4& Light::getColor() const
{
	return this->color;
}

inline const glm::vec2& Light::getPosition() const
{
	return this->position;
}

inline const glm::vec2& Light::getPositionOffset() const
{
	return this->positionOffset;
}

inline float Light::getRange() const
{
	return this->range;
}

inline bool Light::isEnabled() const
{
	return this->enabled;
}

inline float Light::getCastDirection() const
{
	return this->castDirectionRadian;
}

inline void Light::setEffect(const bool state)
{
	this->effect = state;
}

inline bool Light::isEffect() const
{
	return this->effect;
}

}
} }

#endif
