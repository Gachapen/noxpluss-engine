#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>

#include "TrdMeshData.h"


namespace nox {
	namespace app
	{
		namespace graphics
		{
			class TrdMesh
			{


			public:
				TrdMesh(std::vector<vertexData>* vd, std::vector<unsigned int>* id, std::vector<textureData> * td = NULL);
				~TrdMesh();

				void draw(unsigned int programId);

			private:
				std::vector<vertexData> data;
				std::vector<textureData> textures;
				std::vector<unsigned int> indices;

				unsigned int VBO;
				unsigned int IBO;

			};
		}
	}
}
#endif