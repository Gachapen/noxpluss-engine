#ifndef TRDGRAPHICSASSETMANAGER_H
#define TRDGRAPHICSASSETMANAGER_H

#include <map>
#include <nox\app\graphics\ITrdAssetloader.h>

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <nox/app/graphics/TrdSceneLoader.h>



namespace nox {
	namespace app
	{
		namespace graphics
		{
			class TrdGraphicsAssetManager : public ITrdAssetLoader
			{
			public:
				virtual void loadAssetFromFile(std::string filePath, std::string name) override;
				virtual void drawAsset(std::string name, unsigned int programId) override;

			private:

				Assimp::Importer assetImporter;
				std::map<std::string, std::shared_ptr<nox::app::graphics::TrdSceneLoader>>  sceneLoaders;
				
			};
		}
	}
}
#endif