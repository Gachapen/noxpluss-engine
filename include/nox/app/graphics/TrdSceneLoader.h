#ifndef TRDSCENELOADER_H
#define TRDSCENELOADER_H

#include "nox\app\graphics\TrdMesh.h"

#include <assimp\Importer.hpp>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

namespace nox {
	namespace app
	{
		namespace graphics
		{
			class TrdSceneLoader
			{
			public:
				TrdSceneLoader(std::string fileName);
				~TrdSceneLoader();
				void draw(unsigned int programId);

			private:
				std::vector<TrdMesh*> meshes;

				void recursiveProcess(aiNode* node, const aiScene* scene);
				void processMesh(aiMesh* mesh, const aiScene* scene);
				unsigned int loadTexture(const char* fileName);
				std::vector<TrdMesh*>& getMeshes();

			};
		}
	}
}
#endif