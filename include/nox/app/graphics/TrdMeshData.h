#ifndef TRDMESHDATA_H
#define TRDMESHDATA_H

#include <glm\glm.hpp>

namespace nox {
	namespace app
	{
		namespace graphics
		{
			struct vertexData
			{
				glm::vec3 position;
				glm::vec3 normal;
				glm::vec3 tangent;
				glm::vec3 color;
				glm::vec2 uvPosition;
			};

			struct textureData
			{
				unsigned int id;
				unsigned int type;
			};
		}
	}
}
#endif