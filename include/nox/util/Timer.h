/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_TIMER_H_
#define NOX_UTIL_TIMER_H_

namespace nox
{
namespace util
{

/**
 * Simple timer having a length.
 *
 * The Timer has a length specified by the constructor or setTimerLength(). It then has
 * to spend time using spendTime() until enough time has been spent so that it has reached its
 * length. This can be checked with timerReached().
 *
 * A usual case is to call spendTime() each update of a class passing in the time since the previous update. Then
 * check if it has reached its time with timerReached().
 */
template<class DurationType>
class Timer
{
public:
	/**
	 * Construct a timer.
	 */
	Timer();

	/**
	 * Construct a timer.
	 * @param timerLength How long until the timer is finished.
	 */
	Timer(const DurationType& timerLength);

	/**
	 * Set how long until the timer is finished.
	 */
	void setTimerLength(const DurationType& length);

	/**
	 * Spend an amount of time, decreasing the time until the timer is finished.
	 */
	void spendTime(const DurationType& time);

	/**
	 * Reset the spent time to zero.
	 */
	void reset();

	/**
	 * Check if the timer length has been reached.
	 */
	bool timerReached() const;

private:
	DurationType timerLength;
	DurationType remainingTime;
};

template<class DurationType>
Timer<DurationType>::Timer():
	timerLength(0),
	remainingTime(0)
{
}

template<class DurationType>
Timer<DurationType>::Timer(const DurationType& timerLength):
	timerLength(timerLength),
	remainingTime(this->timerLength)
{
}

template<class DurationType>
inline void Timer<DurationType>::setTimerLength(const DurationType& length)
{
	this->timerLength = length;
}

template<class DurationType>
inline void Timer<DurationType>::spendTime(const DurationType& time)
{
	this->remainingTime -= time;
}

template<class DurationType>
inline void Timer<DurationType>::reset()
{
	this->remainingTime = this->timerLength;
}

template<class DurationType>
inline bool Timer<DurationType>::timerReached() const
{
	return (this->remainingTime <= DurationType(0));
}

}
}

#endif
