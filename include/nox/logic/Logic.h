/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_LOGIC_H_
#define NOX_LOGIC_LOGIC_H_

#include "IContext.h"
#include <nox/common/types.h>
#include <nox/common/api.h>
#include <nox/common/compat.h>
#include <nox/app/ApplicationProcess.h>
#include <nox/app/log/Logger.h>
#include <vector>
#include <memory>

namespace nox { namespace logic
{

namespace event
{

class Manager;

}

namespace physics
{

class Simulation;

}

namespace world
{

class Manager;

}

class View;

class NOX_API Logic: public app::ApplicationProcess, public IContext
{
public:
	Logic();
	virtual ~Logic();

	app::IContext* getApplicationContext() const NOX_NOEXCEPT;

	void addView(std::unique_ptr<View> view);
	void setPhysics(std::unique_ptr<physics::Simulation> physics);
	void setWorldHandler(std::unique_ptr<world::Manager> worldHandler);

	void pause(bool pause) override;
	bool isPaused() const override;

	const View* findControllingView(const actor::Identifier& actorId) const override;

	event::IBroadcaster* getEventBroadcaster() override;
	physics::Simulation* getPhysics() override;
	app::resource::IResourceAccess* getResourceAccess() override;
	app::storage::IDataStorage* getDataStorage() override;
	app::log::Logger createLogger() override;

private:
	void onInit() override;
	void onUpdate(const Duration& deltaTime) override;
	void onSuccess() override;
	void onFail() override;
	void onAbort() override;
	void onUpdateFinished(const Duration& alpha) override;

	void destroy();

	bool paused;
	app::log::Logger log;

	std::unique_ptr<event::Manager> eventManager;
	std::vector<std::unique_ptr<View>> viewContainer;
	std::unique_ptr<physics::Simulation> physics;
	std::unique_ptr<world::Manager> world;
};

} }

#endif
