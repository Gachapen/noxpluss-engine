/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_BOX2DUTILS_H_
#define NOX_LOGIC_PHYSICS_BOX2DUTILS_H_

#include "../physics_utils.h"
#include <Box2D/Box2D.h>
#include <nox/util/math/equality.h>
#include <nox/util/Mask.h>

namespace nox { namespace logic { namespace physics
{

/**
 * Calculate the area of a polygon.
 * @param vs Pointer to vertex array.
 * @param count Number of vertices in array.
 * @return The area of the polygon.
 */
float calculatePolygonArea(const b2Vec2* vs, int32 count);

/**
 * Check if the polygon is valid, i.e. ready for Box2D.
 * @param vs Pointer to vertex array.
 * @param count Number of vertices in array.
 * @return If the polygon is valid or not.
 */
bool validPolygon(const b2Vec2* vertices, int32 count);

bool approximatelyEqual(const b2Vec2& a, const b2Vec2& b, float epsilon);

bool pointIsInsideBox(const b2Vec2& point, const b2AABB& box);

util::Mask<PhysicalBodyType> maskFromBodyType(const b2BodyType& type);

bool isEqual(const b2Vec2& a, const b2Vec2& b);


inline bool approximatelyEqual(const b2Vec2& a, const b2Vec2& b, float epsilon)
{
	return (math::approximatelyEqual(a.x, b.x, epsilon) && math::approximatelyEqual(a.y, b.y, epsilon));
}

inline util::Mask<PhysicalBodyType> maskFromBodyType(const b2BodyType& type)
{
	if (type == b2_staticBody)
	{
		return PhysicalBodyType::STATIC;
	}
	else if (type == b2_kinematicBody)
	{
		return PhysicalBodyType::KINEMATIC;
	}
	else if (type == b2_dynamicBody)
	{
		return PhysicalBodyType::DYNAMIC;
	}
	else
	{
		return {};
	}
}

inline bool isEqual(const b2Vec2& a, const b2Vec2& b)
{
	return a.x == b.x && a.y == b.y;
}

} } }

#endif
