/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_TRDTRANSFORM_H_
#define NOX_LOGIC_ACTOR_TRDTRANSFORM_H_

#include "../Component.h"
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

namespace nox { namespace logic { namespace actor
{

/**
 * Place an TrdActor with a position, rotation, and scale.
 * An important component used by many other components.
 *
 * # JSON Description
 * ## Name
 * %TrdTransform
 *
 * ## Properties
 * - __position__:vec3 - Position of the Actor. Default vec3(0, 0, 0).
 * - __rotation__:vec3 - Rotation of the Actor. Default vec3(0, 0, 0).
 * - __scale__:vec3 - Scale of the actor. Default vec3(1, 1, 1).
 */
class TrdTransform: public Component
{
public:
	//! Overloaded functions using this tag do not broadcast the transform change.
	struct NoBroadcast_t {};

	const static IdType NAME;

	virtual ~TrdTransform();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	/**
	 * Get the transform matrix representing the operations scale->rotate->translate.
	 */
	glm::mat4 getTransformMatrix();

	void setPosition(const glm::vec3& position);
	void setPosition(const glm::vec3& position, NoBroadcast_t);

	void setScale(const glm::vec3& scale);
	void setScale(const glm::vec3& scale, NoBroadcast_t);

	void setRotation(const glm::vec3& rotation);
	void setRotation(const glm::vec3& rotation, NoBroadcast_t);

	void setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale);
	void setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale, NoBroadcast_t);

	const glm::vec3& getPosition() const;
	const glm::vec3& getRotation() const;
	const glm::vec3& getScale() const;

	/**
	 * Broadcast a transformation change to other components,
	 * and globally to the EventManager.
	 */
	void broadcastTransformChange();
private:

	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;
};


inline void TrdTransform::setPosition(const glm::vec3& position, NoBroadcast_t)
{
	this->position = position;
}

inline void TrdTransform::setScale(const glm::vec3& scale, NoBroadcast_t)
{
	this->scale = scale;
}

inline void TrdTransform::setRotation(const glm::vec3& rotation, NoBroadcast_t)
{
	this->rotation = rotation;
}

inline void TrdTransform::setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale, NoBroadcast_t)
{
	this->position = position;
	this->scale = scale;
	this->rotation = rotation;
}

inline const glm::vec3& TrdTransform::getPosition() const
{
	return this->position;
}

inline const glm::vec3& TrdTransform::getRotation() const
{
	return this->rotation;
}

inline const glm::vec3& TrdTransform::getScale() const
{
	return this->scale;
}

} } }

#endif
