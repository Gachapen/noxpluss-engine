# README
[The NOX Engine.](https://bitbucket.org/suttungdigital/nox-engine/)

Sample program using the engine is found [here](https://bitbucket.org/suttungdigital/nox-engine-sample).

## Building
This section describes how to set up a build environment and build the engine library for Linux, OS X and Windows.

### Supported Compilers
Because of its heavy use of new ISO C++ (currently C++11 and C++14) features, only the latest stable versions of compilers are supported.

Officially these compilers (and most likely later versions) are supported:

* [GCC](https://gcc.gnu.org/) 4.9
* [LLVM Clang](http://clang.llvm.org/) 3.5
* Apple Clang 6
* Microsoft Visual C++ 12 2014

Other compilers should work as long as they support the latest ISO C++ features.

### Dependencies
#### Configure
* [CMake](http://www.cmake.org/)

#### Libraries
* [SDL2](https://www.libsdl.org/download-2.0.php)
* [SDL2_image](https://www.libsdl.org/projects/SDL_image/)
* OpenGL
* [GLEW](http://glew.sourceforge.net/)
* [Boost](http://www.boost.org/) (Geometry, Locale and Filesystem libraries)
* [libogg](http://xiph.org/ogg/)
* [libvorbis](http://xiph.org/vorbis/) (vorbisfile)
* [OpenAL Soft](http://kcat.strangesoft.net/openal.html) (or any other OpenAL implementation)

#### Build
Your favorite build tools.
For this guide we assume that you are using Make for Linux, Xcode for OS X and Visual Studio for Windows.
In any case, the steps should be similar (see CMake documentation for help).

### Preparing System
#### Linux
Install CMake and the libraries listed above through your system's package manager (or build them yourself).

#### OS X
Install CMake and the libraries listed above.
To easily accomplish this you can use [Homebrew](http://brew.sh/).
You can also install CMake [directly from their website](http://www.cmake.org/).

#### Windows
Install CMake from [their website](http://www.cmake.org/).
You can either download and build all the libraries yourself, or use our precompiled collection [located here](https://bitbucket.org/suttungdigital/windows-libraries).

In any case, you need to set up CMake on your system to find the libraries.
To do this, set the environment variable `CMAKE_PREFIX_PATH` to point to the library directory.
Alternatively you can set `CMAKE_PREFIX_PATH` only for NOX Engine when configuring the CMake build.

The library directory should have one `include` directory with all the header files, and one `lib` directory with all the lib files.

To easily run applications using the libraries, set the directory of the dll files to the `PATH` environment variable.

If you want to use our precompiled libraries, clone the repository and run the `create_msvc_usr.ps1` script (right click -> Run in PowerShell).
You might need to allow running scripts on your system.
To do this, open a PowerShell terminal and run `Set-ExecutionPolicy Unrestricted -Scope CurrentUser`.
When the script is done (the terminal has been closed), there will be a new `usr` directory next to the script.
Set the `CMAKE_PREFIX_PATH` to point to this.
Also set `PATH` to point to `usr\bin\x64` and `usr\bin\x86` to be able to use both the 64-bit and 32-bit dll's.

### Getting the Sources
We recommend using a directory structure where you have a directory for the project, e.g. named `nox-engine`, and two subdirectories called `source` and `build`.
For this guide, we assume you use this structure.

To get the sources:

1. `git clone <nox-engine-url> source`
2. `cd source`
3. `git submodule update --init --recursive`

You need to use the `git submodule update` command, otherwise the third party libraries won't be available.

After pulling a new version down, you should run `git submodule update --init --recursive` again, so that the third party libraries are properly checked out. The `--init` isn't strictly necessary, but it will ensure that you initialize any new submodules.

### Building
To configure the project, run `cmake ../source` from the build directory, or use the CMake GUI tool and set the proper directories.

If you want to configure any options, add `-D<option>` to the `cmake` command.
Use `cat CMakeCache.txt | grep -B1 NOX` after you have run `cmake` once to list all available options.
If you're using the CMake GUI, you can edit the options from there.
Example use of option:
`cmake -DNOX_BUILD_TEST=OFF ../source`.

After pulling down a new version of the source, you should run the `cmake` command over again.

You can generate different types of projects, see http://www.cmake.org/cmake/help/v3.1/manual/cmake-generators.7.html for which and how.
Below are basic instructions for the various platforms.

#### Linux
After configuring with CMake, run `make` in the build directory, or open your generated project.
Use `make -j<number-of-threads>` to build with multiple threads.

#### OS X
If you want to use Xcode, enable the Xcode generator by adding `-GXcode` to the `cmake` command.
Example: `cmake -GXcode ../source`.
From the GUI you can select to use Xcode when configuring.

After configuring with CMake, run `make` in the build directory, or use your generated project.
Use `make -j<number-of-threads>` to build with multiple threads.

#### Windows
If you want to use Visual Studio, enable the Visual Studio generator by adding `-G"Visual Studio 12 2013"` to the `cmake` command.
From the GUI you can select to use Visual Studio when configuring.
To create a 64-bit project, replace `-G"Visual Studio 12 2013"` with `-G"Visual Studio 12 2013 Win64"`.
Example: `cmake -G"Visual Studio 12 2013" ..\source`.

If you didn't set the `CMAKE_PREFIX_PATH` environment variable (see Windows under Preparing System), add `-DCMAKE_PREFIX_PATH=\path\to\your\librarydir` to the `cmake` command, or set the variable in the GUI.
Example: `cmake -DCMAKE_PREFIX_PATH=\path\to\your\librarydir ..\source`

After configuring with CMake, run `make` in the build directory, or use your generated project/solution.

## Using the Engine
The easiest way to use the engine is to set up your own CMake project, adding the engine as a git submodule, including it in your CMake project with `add_subdirectory` and linking with the `target_link_libraries` command.
See [NOX Engine Sample](https://bitbucket.org/suttungdigital/nox-engine-sample) for an example project using the engine as a subproject.

## Documentation
You can generate html documentation by using [Doxygen](http://www.stack.nl/~dimitri/doxygen/) in the `docs` directory.

## License
NOX Engine is licensed under the MIT license, basically allowing you to use it however you want.
See below for the license.

```
Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
