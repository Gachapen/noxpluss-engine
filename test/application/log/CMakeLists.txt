add_executable(test-log
	TestLog.cpp
)

target_link_libraries(test-log
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

add_test(
	NAME test-log
	COMMAND test-log
)
